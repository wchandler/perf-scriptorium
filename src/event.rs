use libc::pid_t;
use scroll::Pread;
use std::fmt;

/// perf_event_type
pub const PERF_RECORD_MMAP: u32 = 1;
pub const PERF_RECORD_LOST: u32 = 2;
pub const PERF_RECORD_COMM: u32 = 3;
pub const PERF_RECORD_EXIT: u32 = 4;
pub const PERF_RECORD_THROTTLE: u32 = 5;
pub const PERF_RECORD_UNTHROTTLE: u32 = 6;
pub const PERF_RECORD_FORK: u32 = 7;
pub const PERF_RECORD_READ: u32 = 8;
pub const PERF_RECORD_SAMPLE: u32 = 9;
pub const PERF_RECORD_MMAP2: u32 = 10;
pub const PERF_RECORD_AUX: u32 = 11;
pub const PERF_RECORD_ITRACE_START: u32 = 12;
pub const PERF_RECORD_LOST_SAMPLES: u32 = 13;
pub const PERF_RECORD_SWITCH: u32 = 14;
pub const PERF_RECORD_SWITCH_CPU_WIDE: u32 = 15;
pub const PERF_RECORD_NAMESPACES: u32 = 16;
pub const PERF_RECORD_KSYMBOL: u32 = 17;
pub const PERF_RECORD_BPF_EVENT: u32 = 18;
pub const PERF_RECORD_CGROUP: u32 = 19;
pub const PERF_RECORD_TEXT_POKE: u32 = 20;
pub const PERF_RECORD_AUX_OUTPUT_HW_ID: u32 = 21;
pub const PERF_RECORD_MAX: u32 = 22;

/// perf_user_event_type
/// tools/lib/perf/include/perf/event.h
pub const PERF_RECORD_USER_TYPE_START: u32 = 64;
pub const PERF_RECORD_HEADER_ATTR: u32 = 64;
pub const PERF_RECORD_HEADER_EVENT_TYPE: u32 = 65;
pub const PERF_RECORD_HEADER_TRACING_DATA: u32 = 66;
pub const PERF_RECORD_HEADER_BUILD_ID: u32 = 67;
pub const PERF_RECORD_FINISHED_ROUND: u32 = 68;
pub const PERF_RECORD_ID_INDEX: u32 = 69;
pub const PERF_RECORD_AUXTRACE_INFO: u32 = 70;
pub const PERF_RECORD_AUXTRACE: u32 = 71;
pub const PERF_RECORD_AUXTRACE_ERROR: u32 = 72;
pub const PERF_RECORD_THREAD_MAP: u32 = 73;
pub const PERF_RECORD_CPU_MAP: u32 = 74;
pub const PERF_RECORD_STAT_CONFIG: u32 = 75;
pub const PERF_RECORD_STAT: u32 = 76;
pub const PERF_RECORD_STAT_ROUND: u32 = 77;
pub const PERF_RECORD_EVENT_UPDATE: u32 = 78;
pub const PERF_RECORD_TIME_CONV: u32 = 79;
pub const PERF_RECORD_HEADER_FEATURE: u32 = 80;
pub const PERF_RECORD_COMPRESSED: u32 = 81;
pub const PERF_RECORD_FINISHED_INIT: u32 = 82;
pub const PERF_RECORD_HEADER_MAX: u32 = 83;

pub fn record_to_s(record: u32) -> &'static str {
    match record {
        PERF_RECORD_MMAP => "PERF_RECORD_MMAP",
        PERF_RECORD_LOST => "PERF_RECORD_LOST",
        PERF_RECORD_COMM => "PERF_RECORD_COMM",
        PERF_RECORD_EXIT => "PERF_RECORD_EXIT",
        PERF_RECORD_THROTTLE => "PERF_RECORD_THROTTLE",
        PERF_RECORD_UNTHROTTLE => "PERF_RECORD_UNTHROTTLE",
        PERF_RECORD_FORK => "PERF_RECORD_FORK",
        PERF_RECORD_READ => "PERF_RECORD_READ",
        PERF_RECORD_SAMPLE => "PERF_RECORD_SAMPLE",
        PERF_RECORD_MMAP2 => "PERF_RECORD_MMAP2",
        PERF_RECORD_AUX => "PERF_RECORD_AUX",
        PERF_RECORD_ITRACE_START => "PERF_RECORD_ITRACE_START",
        PERF_RECORD_LOST_SAMPLES => "PERF_RECORD_LOST_SAMPLES",
        PERF_RECORD_SWITCH => "PERF_RECORD_SWITCH",
        PERF_RECORD_SWITCH_CPU_WIDE => "PERF_RECORD_SWITCH_CPU_WIDE",
        PERF_RECORD_NAMESPACES => "PERF_RECORD_NAMESPACES",
        PERF_RECORD_KSYMBOL => "PERF_RECORD_KSYMBOL",
        PERF_RECORD_BPF_EVENT => "PERF_RECORD_BPF_EVENT",
        PERF_RECORD_CGROUP => "PERF_RECORD_CGROUP",
        PERF_RECORD_TEXT_POKE => "PERF_RECORD_TEXT_POKE",
        PERF_RECORD_AUX_OUTPUT_HW_ID => "PERF_RECORD_AUX_OUTPUT_HW_ID",
        PERF_RECORD_HEADER_ATTR => "PERF_RECORD_HEADER_ATTR",
        PERF_RECORD_HEADER_EVENT_TYPE => "PERF_RECORD_HEADER_EVENT_TYPE",
        PERF_RECORD_HEADER_TRACING_DATA => "PERF_RECORD_HEADER_TRACING_DATA",
        PERF_RECORD_HEADER_BUILD_ID => "PERF_RECORD_HEADER_BUILD_ID",
        PERF_RECORD_FINISHED_ROUND => "PERF_RECORD_FINISHED_ROUND",
        PERF_RECORD_ID_INDEX => "PERF_RECORD_ID_INDEX",
        PERF_RECORD_AUXTRACE_INFO => "PERF_RECORD_AUXTRACE_INFO",
        PERF_RECORD_AUXTRACE => "PERF_RECORD_AUXTRACE",
        PERF_RECORD_AUXTRACE_ERROR => "PERF_RECORD_AUXTRACE_ERROR",
        PERF_RECORD_THREAD_MAP => "PERF_RECORD_THREAD_MAP",
        PERF_RECORD_CPU_MAP => "PERF_RECORD_CPU_MAP",
        PERF_RECORD_STAT_CONFIG => "PERF_RECORD_STAT_CONFIG",
        PERF_RECORD_STAT => "PERF_RECORD_STAT",
        PERF_RECORD_STAT_ROUND => "PERF_RECORD_STAT_ROUND",
        PERF_RECORD_EVENT_UPDATE => "PERF_RECORD_EVENT_UPDATE",
        PERF_RECORD_TIME_CONV => "PERF_RECORD_TIME_CONV",
        PERF_RECORD_HEADER_FEATURE => "PERF_RECORD_HEADER_FEATURE",
        PERF_RECORD_COMPRESSED => "PERF_RECORD_COMPRESSED",
        PERF_RECORD_FINISHED_INIT => "PERF_RECORD_FINISHED_INIT",
        _ => "unknown event type",
    }
}

/// perf_event.h
/// List types of data that may be sampled
pub const PERF_SAMPLE_IP: u32 = 1 << 0;
pub const PERF_SAMPLE_TID: u32 = 1 << 1;
pub const PERF_SAMPLE_TIME: u32 = 1 << 2;
pub const PERF_SAMPLE_ADDR: u32 = 1 << 3;
pub const PERF_SAMPLE_READ: u32 = 1 << 4;
pub const PERF_SAMPLE_CALLCHAIN: u32 = 1 << 5;
pub const PERF_SAMPLE_ID: u32 = 1 << 6;
pub const PERF_SAMPLE_CPU: u32 = 1 << 7;
pub const PERF_SAMPLE_PERIOD: u32 = 1 << 8;
pub const PERF_SAMPLE_STREAM_ID: u32 = 1 << 9;
pub const PERF_SAMPLE_RAW: u32 = 1 << 10;
pub const PERF_SAMPLE_BRANCH_STACK: u32 = 1 << 11;
pub const PERF_SAMPLE_REGS_USER: u32 = 1 << 12;
pub const PERF_SAMPLE_STACK_USER: u32 = 1 << 13;
pub const PERF_SAMPLE_WEIGHT: u32 = 1 << 14;
pub const PERF_SAMPLE_DATA_SRC: u32 = 1 << 15;
pub const PERF_SAMPLE_IDENTIFIER: u32 = 1 << 16;
pub const PERF_SAMPLE_TRANSACTION: u32 = 1 << 17;
pub const PERF_SAMPLE_REGS_INTR: u32 = 1 << 18;
pub const PERF_SAMPLE_PHYS_ADDR: u32 = 1 << 19;
pub const PERF_SAMPLE_AUX: u32 = 1 << 20;
pub const PERF_SAMPLE_CGROUP: u32 = 1 << 21;
pub const PERF_SAMPLE_DATA_PAGE_SIZE: u32 = 1 << 22;
pub const PERF_SAMPLE_CODE_PAGE_SIZE: u32 = 1 << 23;
pub const PERF_SAMPLE_WEIGHT_STRUCT: u32 = 1 << 24;
pub const PERF_SAMPLE_MAX: u32 = 1 << 25;

/// include/uapi/linux/perf_event.h
#[derive(Pread)]
pub struct PerfEventHeader {
    pub ty: u32,
    pub misc: u16,
    pub size: u16,
}

impl fmt::Debug for PerfEventHeader {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("PerfEventHeader")
            .field("type", &format_args!("0x{:x} - {}", self.ty, self.ty))
            .field("misc", &format_args!("0x{:x} - {}", self.misc, self.misc))
            .field("size", &format_args!("0x{:x} - {}", self.size, self.size))
            .finish()
    }
}

#[derive(Pread, Debug)]
#[repr(C)]
pub struct PerfEventAttr {
    ty: u32,
    size: u32,
    config: u64,
    sample_freq: u64, // union
    sample_type: u64,
    read_format: u64,
    flags: u64, // bitflags
    wakeup_events: u32,
    bp_type: u32,
    config1: u64, // union
    config2: u64, // union
    branch_sample_type: u64,
    sample_regs_user: u64,
    sample_stack_user: u32,
    clockid: i32,
    sample_regs_intr: u64,
    aux_watermark: u32,
    sample_max_stack: u16,
    reserved_2: u16,
    aux_sample_size: u32,
    reserverd_3: u32,
    sig_data: u64,
    config3: u64,
}

impl PerfEventAttr {
    #[inline]
    pub fn disabled(&self) -> bool {
        self.flags & 1 != 0
    }

    #[inline]
    pub fn inherit(&self) -> bool {
        self.flags & 1 << 1 != 0
    }

    #[inline]
    pub fn pinned(&self) -> bool {
        self.flags & 1 << 2 != 0
    }

    #[inline]
    pub fn exlusive(&self) -> bool {
        self.flags & 1 << 3 != 0
    }

    #[inline]
    pub fn exclusive_user(&self) -> bool {
        self.flags & 1 << 4 != 0
    }

    #[inline]
    pub fn exclusive_kernel(&self) -> bool {
        self.flags & 1 << 5 != 0
    }

    #[inline]
    pub fn exclusive_hv(&self) -> bool {
        self.flags & 1 << 6 != 0
    }

    #[inline]
    pub fn exclusive_idle(&self) -> bool {
        self.flags & 1 << 7 != 0
    }

    #[inline]
    pub fn mmap(&self) -> bool {
        self.flags & 1 << 8 != 0
    }

    #[inline]
    pub fn comm(&self) -> bool {
        self.flags & 1 << 9 != 0
    }

    #[inline]
    pub fn freq(&self) -> bool {
        self.flags & 1 << 10 != 0
    }

    #[inline]
    pub fn inherit_stat(&self) -> bool {
        self.flags & 1 << 11 != 0
    }

    #[inline]
    pub fn enable_on_exec(&self) -> bool {
        self.flags & 1 << 12 != 0
    }

    #[inline]
    pub fn task(&self) -> bool {
        self.flags & 1 << 13 != 0
    }

    #[inline]
    pub fn watermark(&self) -> bool {
        self.flags & 1 << 14 != 0
    }

    #[inline]
    pub fn precise_ip(&self) -> u8 {
        (self.flags & 2 << 15) as u8
    }

    #[inline]
    pub fn mmap_data(&self) -> bool {
        self.flags & 1 << 17 != 0
    }

    #[inline]
    pub fn sample_id_all(&self) -> bool {
        self.flags & 1 << 18 != 0
    }

    #[inline]
    pub fn exclude_host(&self) -> bool {
        self.flags & 1 << 19 != 0
    }

    #[inline]
    pub fn exclude_guest(&self) -> bool {
        self.flags & 1 << 20 != 0
    }

    #[inline]
    pub fn exclude_callchain_kernel(&self) -> bool {
        self.flags & 1 << 21 != 0
    }

    #[inline]
    pub fn exclude_callchain_user(&self) -> bool {
        self.flags & 1 << 22 != 0
    }

    #[inline]
    pub fn mmap2(&self) -> bool {
        self.flags & 1 << 23 != 0
    }

    #[inline]
    pub fn comm_exec(&self) -> bool {
        self.flags & 1 << 24 != 0
    }

    #[inline]
    pub fn use_clockid(&self) -> bool {
        self.flags & 1 << 25 != 0
    }

    #[inline]
    pub fn context_switch(&self) -> bool {
        self.flags & 1 << 26 != 0
    }

    #[inline]
    pub fn write_backward(&self) -> bool {
        self.flags & 1 << 27 != 0
    }

    #[inline]
    pub fn namespaces(&self) -> bool {
        self.flags & 1 << 28 != 0
    }

    #[inline]
    pub fn ksymbol(&self) -> bool {
        self.flags & 1 << 29 != 0
    }

    #[inline]
    pub fn bpf_event(&self) -> bool {
        self.flags & 1 << 30 != 0
    }

    #[inline]
    pub fn aux_output(&self) -> bool {
        self.flags & 1 << 31 != 0
    }

    #[inline]
    pub fn cgroup(&self) -> bool {
        self.flags & 1 << 32 != 0
    }

    #[inline]
    pub fn text_poke(&self) -> bool {
        self.flags & 1 << 33 != 0
    }

    #[inline]
    pub fn build_id(&self) -> bool {
        self.flags & 1 << 34 != 0
    }

    #[inline]
    pub fn inherit_thread(&self) -> bool {
        self.flags & 1 << 35 != 0
    }

    #[inline]
    pub fn remove_on_exec(&self) -> bool {
        self.flags & 1 << 36 != 0
    }

    #[inline]
    pub fn sigtrap(&self) -> bool {
        self.flags & 1 << 37 != 0
    }
}

#[derive(Debug)]
pub struct BuildIdEvent<'a> {
    header: PerfEventHeader,
    pid: pid_t,
    filename: &'a str,
}

#[derive(Debug)]
pub struct PerfRecordMmap<'a> {
    header: PerfEventHeader,
    pid: u32,
    tid: u32,
    addr: u64,
    len: u64,
    pgoff: u64,
    filename: &'a [u8],
}

#[derive(Debug)]
pub struct PerfRecordMmap2<'a> {
    header: PerfEventHeader,
    pid: u32,
    tid: u32,
    addr: u64,
    len: u64,
    pgoff: u64,
    maj: u32, // TODO union
    min: u32,
    ino: u64,
    ino_generation: u32,
    prot: u32,
    flags: u32,
    filename: &'a [u8],
}

#[derive(Pread)]
pub struct PerfRecordComm {
    header: PerfEventHeader,
    pid: u32,
    tid: u32,
    comm: [u8; 16],
}

#[derive(Pread, Debug)]
pub struct PerfRecordTimeConv {
    header: PerfEventHeader,
    time_shift: u64,
    time_mult: u64,
    time_zero: u64,
    time_cycles: u64,
    cap_user_time_zero: u8,
    cap_user_time_short: u8,
    reserved: [u8; 6],
}
