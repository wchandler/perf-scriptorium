use event::*;
use event::{PerfEventAttr, PerfEventHeader, PerfRecordTimeConv};

use bstr::{BStr, BString};
use libc::pid_t;
use scroll::Pread;
use std::collections::BTreeMap;
use std::ffi::CStr;
use std::fmt;
use std::fs;
use std::mem;
use std::str;

mod event;

pub const PERF_MAGIC: &[u8; 8] = b"PERFILE2";
pub const PERF_MAGIC_OTHER_ENDIAN: &[u8; 8] = b"2ELIFREP";

/// Available perf header versions.
pub const PERF_HEADER_VERSION_1: u32 = 1;
pub const PERF_HEADER_VERSION_2: u32 = 2;

/// Header flags
pub const HEADER_RESERVED: u32 = 0;
pub const HEADER_FIRST_FEATURE: u32 = 1;
pub const HEADER_TRACING_DATA: u32 = 1;
pub const HEADER_BUILD_ID: u32 = 2;
pub const HEADER_HOSTNAME: u32 = 3;
pub const HEADER_OSRELEASE: u32 = 4;
pub const HEADER_VERSION: u32 = 5;
pub const HEADER_ARCH: u32 = 6;
pub const HEADER_NRCPUS: u32 = 7;
pub const HEADER_CPUDESC: u32 = 8;
pub const HEADER_CPUID: u32 = 9;
pub const HEADER_TOTAL_MEM: u32 = 10;
pub const HEADER_CMDLINE: u32 = 11;
pub const HEADER_EVENT_DESC: u32 = 12;
pub const HEADER_CPU_TOPOLOGY: u32 = 13;
pub const HEADER_NUMA_TOPOLOGY: u32 = 14;
pub const HEADER_BRANCH_STACK: u32 = 15;
pub const HEADER_PMU_MAPPINGS: u32 = 16;
pub const HEADER_GROUP_DESC: u32 = 17;
pub const HEADER_AUXTRACE: u32 = 18;
pub const HEADER_STAT: u32 = 19;
pub const HEADER_CACHE: u32 = 20;
pub const HEADER_SAMPLE_TIME: u32 = 21;
pub const HEADER_MEM_TOPOLOGY: u32 = 22;
pub const HEADER_CLOCKID: u32 = 23;
pub const HEADER_DIR_FORMAT: u32 = 24;
pub const HEADER_BPF_PROG_INFO: u32 = 25;
pub const HEADER_BPF_BTF: u32 = 26;
pub const HEADER_COMPRESSED: u32 = 27;
pub const HEADER_CPU_PMU_CAPS: u32 = 28;
pub const HEADER_CLOCK_DATA: u32 = 29;
pub const HEADER_HYBRID_TOPOLOGY: u32 = 30;
pub const HEADER_PMU_CAPS: u32 = 31;
pub const HEADER_LAST_FEATURE: u32 = 32;

/// Perf attr types see perf_event.h
pub const PERF_TYPE_HARDWARE: u32 = 0;
pub const PERF_TYPE_SOFTWARE: u32 = 1;
pub const PERF_TYPE_TRACEPOINT: u32 = 2;
pub const PERF_TYPE_HW_CACHE: u32 = 3;
pub const PERF_TYPE_RAW: u32 = 4;
pub const PERF_TYPE_BREAKPOINT: u32 = 0;
pub const PERF_TYPE_MAX: u32 = 6;

const fn header_to_str(header: u32) -> &'static str {
    match header {
        HEADER_TRACING_DATA => "Tracing Data",
        HEADER_BUILD_ID => "Build ID",
        HEADER_HOSTNAME => "Hostname",
        HEADER_OSRELEASE => "OS Release",
        HEADER_VERSION => "Perf Version",
        HEADER_ARCH => "CPU Architecture",
        HEADER_NRCPUS => "CPU Count",
        HEADER_CPUDESC => "CPU Description",
        HEADER_CPUID => "CPU ID",
        HEADER_TOTAL_MEM => "Total Memory (KiB)",
        HEADER_CMDLINE => "Command Line",
        HEADER_EVENT_DESC => "Event Description",
        HEADER_CPU_TOPOLOGY => "CPU Topology",
        HEADER_NUMA_TOPOLOGY => "NUMA Topology",
        HEADER_BRANCH_STACK => "Branch Stack",
        HEADER_PMU_MAPPINGS => "PMU Mappings",
        HEADER_GROUP_DESC => "CGroup Description",
        HEADER_AUXTRACE => "Auxtrace",
        HEADER_STAT => "Perf Stat",
        HEADER_CACHE => "Cache",
        HEADER_SAMPLE_TIME => "Sample Times",
        HEADER_MEM_TOPOLOGY => "Memory Topology",
        HEADER_CLOCKID => "Clock ID",
        HEADER_DIR_FORMAT => "Directory Format Version",
        HEADER_BPF_PROG_INFO => "BPF Program Info",
        HEADER_BPF_BTF => "BPF BTF Information",
        HEADER_COMPRESSED => "Compression",
        HEADER_CPU_PMU_CAPS => "CPU PMU Capabilities",
        HEADER_CLOCK_DATA => "Clock Data",
        HEADER_HYBRID_TOPOLOGY => "Hybrid Topology",
        HEADER_PMU_CAPS => "PMU Capabilities",
        _ => "Unknown header",
    }
}

/// Format of perf.data
/// - perf_file_header
/// - ids associated with attr
/// - attrs
/// - event list
/// - headers (trailers?)

/// tools/perf/util/header.h
/// See perf_session__do_write_header
#[derive(Pread)]
struct PerfFileHeader {
    magic: [u8; 8],
    size: u64,
    attr_size: u64,
    attrs: PerfFileSection,
    data: PerfFileSection,
    event_types: PerfFileSection, // Unused since 3.12 44b3c57
    flags: u64,
    flags1: [u64; 3],
}

impl fmt::Debug for PerfFileHeader {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("PerfHeader")
            .field("magic", &str::from_utf8(&self.magic).unwrap())
            .field("size", &format_args!("0x{:x} - {}", self.size, self.size))
            .field(
                "attr_size",
                &format_args!("0x{:x} - {}", self.attr_size, self.attr_size),
            )
            .field("attrs", &self.attrs)
            .field("data", &self.data)
            .field("event_types", &self.event_types)
            .field("flags", &format_args!("0b{:032b}", self.flags))
            .field(
                "flags1",
                &format_args!(
                    "[ \n    0b{:032b},\n    0b{:032b},\n    0b{:032b},\n]",
                    self.flags1[0], self.flags1[1], self.flags1[2]
                ),
            )
            .finish()
    }
}

#[repr(C)]
#[derive(Pread)]
struct PerfFileSection {
    offset: u64,
    size: u64,
}

impl fmt::Debug for PerfFileSection {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("PerfHeaderSection")
            .field(
                "offset",
                &format_args!("0x{:x} - {}", self.offset, self.offset),
            )
            .field("size", &format_args!("0x{:x} - {}", self.size, self.size))
            .finish()
    }
}

#[derive(Pread, Debug)]
struct NrCpus {
    nr_cpus_available: u32,
    nr_cpus_online: u32,
}

fn check_flag(num: u32, flags: u64) -> bool {
    1 & (flags >> (num as u64 & (64 - 1))) > 0
}

fn extract_header_bytes<'a>(bytes: &'a [u8], section: &PerfFileSection) -> &'a [u8] {
    let size = section.size as usize;
    let offset = section.offset as usize;

    &bytes[offset..offset + size]
}

fn extract_string_header(bytes: &[u8]) -> (&BStr, u32) {
    let len: u32 = bytes.pread(0).unwrap();

    let s = CStr::from_bytes_until_nul(&bytes[4..]).unwrap();
    (s.to_bytes().into(), len)
}

fn extract_string_list_header(bytes: &[u8]) -> (BString, u32) {
    let mut bytes = bytes;

    let mut len = 0;

    let str_ct: u32 = bytes.pread(0).unwrap();
    bytes = &bytes[4..];
    len += 4;

    let mut strs: Vec<&BStr> = Vec::with_capacity(str_ct as usize);
    for _ in 0..str_ct {
        let str_len: u32 = bytes.pread(0).unwrap();
        bytes = &bytes[4..];

        let s = CStr::from_bytes_until_nul(&bytes[..str_len as usize]).unwrap();
        strs.push(s.to_bytes().into());

        bytes = &bytes[str_len as usize..];
        len += 4 + str_len;
    }

    (bstr::join(" ", strs).into(), len)
}

fn extract_build_id(bytes: &[u8]) {
    let mut bytes = bytes;

    let _event_header: PerfEventHeader = bytes.pread(0).unwrap();
    bytes = &bytes[mem::size_of::<PerfEventHeader>()..];

    let pid: pid_t = bytes.pread(0).unwrap();
    bytes = &bytes[mem::size_of::<pid_t>()..];

    let build_id: [u8; 20] = bytes.pread(0).unwrap();
    let build_id_size: u8 = bytes.pread(20).unwrap();
    // Skip reserved bytes.
    bytes = &bytes[24..];

    bytes = &bytes[..build_id_size as usize];
    let fname = CStr::from_bytes_until_nul(bytes).unwrap();

    println!("  Pid: {pid}");
    println!("  Build ID: {}", hex::encode(build_id));
    println!("  Filename: {}", BStr::new(fname.to_bytes()));
}

fn extract_pmu_mappings(bytes: &[u8]) {
    let mut bytes = bytes;

    let pmu_ct: u32 = bytes.pread(0).unwrap();
    bytes = &bytes[4..];

    for _ in 0..pmu_ct {
        let pmu_type: u32 = bytes.pread(0).unwrap();
        bytes = &bytes[4..];

        let (name, len) = extract_string_header(bytes);
        bytes = &bytes[len as usize + 4..];
        println!("  {pmu_type:<2}: {name}");
    }
}

fn extract_pmu_cpu_caps(bytes: &[u8]) {
    let mut bytes = bytes;

    let pmu_cpu_cap_ct: u32 = bytes.pread(0).unwrap();
    bytes = &bytes[4..];

    for _ in 0..pmu_cpu_cap_ct {
        let (pmu_name, len) = extract_string_header(bytes);
        bytes = &bytes[len as usize + 4..];

        let (cpus, len) = extract_string_header(bytes);
        bytes = &bytes[len as usize + 4..];

        println!("  {pmu_name}: {cpus}");
    }
}

#[derive(Default)]
struct PmuCaps<'a> {
    caps: Vec<(&'a BStr, &'a BStr)>,
    pmu_name: &'a BStr,
}

fn extract_pmu_caps(bytes: &[u8]) {
    let mut bytes = bytes;

    let pmu_ct: u32 = bytes.pread(0).unwrap();
    bytes = &bytes[4..];

    let mut pmus: Vec<PmuCaps> = Vec::with_capacity(pmu_ct as usize);

    for _ in 0..pmu_ct {
        let nr_caps: u32 = bytes.pread(0).unwrap();
        bytes = &bytes[4..];

        let mut pmu = PmuCaps {
            caps: Vec::with_capacity(nr_caps as usize),
            ..Default::default()
        };

        for _ in 0..nr_caps {
            let (cap_name, len) = extract_string_header(bytes);
            bytes = &bytes[len as usize + 4..];

            let (cap_value, len) = extract_string_header(bytes);
            bytes = &bytes[len as usize + 4..];

            pmu.caps.push((cap_name, cap_value));
        }

        let (pmu_name, len) = extract_string_header(bytes);
        bytes = &bytes[len as usize + 4..];

        pmu.pmu_name = pmu_name;
        pmus.push(pmu);
    }

    for pmu in pmus {
        println!("  PMU: {}", pmu.pmu_name);
        for (cap_name, cap_value) in pmu.caps {
            println!("    {cap_name}: {cap_value}");
        }
    }
}

struct CacheInfo<'a> {
    line_size: u32,
    sets: u32,
    ways: u32,
    ty: &'a BStr,
    size: &'a BStr,
    map: &'a BStr,
}

fn extract_cache(bytes: &[u8]) -> BTreeMap<u32, Vec<CacheInfo>> {
    let mut bytes = bytes;

    let version: u32 = bytes.pread(0).unwrap();
    assert_eq!(version, 1);
    let cache_ct: u32 = bytes.pread(4).unwrap();
    bytes = &bytes[8..];
    let mut levels = BTreeMap::new();

    for _ in 0..cache_ct {
        let level: u32 = bytes.pread(0).unwrap();
        let line_size: u32 = bytes.pread(4).unwrap();
        let sets: u32 = bytes.pread(8).unwrap();
        let ways: u32 = bytes.pread(12).unwrap();
        bytes = &bytes[16..];

        let (ty, len) = extract_string_header(bytes);
        bytes = &bytes[len as usize + 4..];

        let (size, len) = extract_string_header(bytes);
        bytes = &bytes[len as usize + 4..];

        let (map, len) = extract_string_header(bytes);
        bytes = &bytes[len as usize + 4..];

        let cache = CacheInfo {
            line_size,
            sets,
            ways,
            ty,
            size,
            map,
        };

        let level = levels.entry(level).or_insert_with(Vec::new);
        level.push(cache);
    }

    levels
}

#[derive(Default)]
struct CpuInfo {
    core: u32,
    socket: u32,
    die: u32,
}

fn extract_cpu_topology(bytes: &[u8], nr_cpus: u32) {
    let mut bytes = bytes;

    let (cores, len) = extract_string_list_header(bytes);
    bytes = &bytes[len as usize..];

    let (threads, len) = extract_string_list_header(bytes);
    bytes = &bytes[len as usize..];

    println!("  Cores: {cores}");
    println!("  Threads: {threads}");

    let mut cpus = Vec::with_capacity(nr_cpus as usize);
    for _ in 0..nr_cpus {
        let core: u32 = bytes.pread(0).unwrap();
        let socket: u32 = bytes.pread(4).unwrap();
        bytes = &bytes[8..];

        let cpu = CpuInfo {
            core,
            socket,
            die: 0,
        };

        cpus.push(cpu);
    }

    let (dies, len) = extract_string_list_header(bytes);
    bytes = &bytes[len as usize..];

    println!("  Dies: {dies}");

    for i in 0..nr_cpus {
        let die: u32 = bytes.pread(0).unwrap();
        bytes = &bytes[4..];

        let cpu = &mut cpus[i as usize];
        cpu.die = die;
    }

    for (i, cpu) in cpus.iter().enumerate() {
        println!(
            "  CPU: {i}, Core ID: {}, Die ID: {}, Socket ID: {}",
            cpu.core, cpu.die, cpu.socket
        );
    }
}

fn extract_event_desc(bytes: &[u8]) {
    let mut bytes = bytes;

    let event_ct: u32 = bytes.pread(0).unwrap();
    let attr_size: u32 = bytes.pread(4).unwrap();
    bytes = &bytes[8..];

    for _ in 0..event_ct {
        let attr_bytes = &bytes[..mem::size_of::<PerfEventAttr>()];
        let event_attr: PerfEventAttr = attr_bytes.pread(0).unwrap();
        bytes = &bytes[attr_size as usize..];
        dbg!(attr_size, event_attr);

        let nr_ids: u32 = bytes.pread(0).unwrap();
        let (event_string, len) = extract_string_header(&bytes[4..]);
        bytes = &bytes[4 + len as usize..];

        let mut ids = Vec::with_capacity(nr_ids as usize);

        for _ in 0..nr_ids {
            let id: u64 = bytes.pread(0).unwrap();
            ids.push(id);
            bytes = &bytes[8..];
        }
        dbg!(nr_ids, event_string, ids);
    }
}

fn extract_group_desc(bytes: &[u8]) {
    let mut bytes = bytes;

    let group_ct: u32 = bytes.pread(0).unwrap();
    bytes = &bytes[4..];

    for _ in 0..group_ct {
        let (name, len) = extract_string_header(bytes);
        bytes = &bytes[len as usize + 4..];

        let leader_idx: u32 = bytes.pread(0).unwrap();
        bytes = &bytes[4..];

        let nr_members: u32 = bytes.pread(0).unwrap();
        bytes = &bytes[4..];

        dbg!(name, leader_idx, nr_members);
    }
}

struct HeaderIter<'a> {
    idx: u32,
    offset: usize,
    flags: u64,
    bytes: &'a [u8],
}

impl<'a> HeaderIter<'a> {
    pub fn new(flags: u64, bytes: &'a [u8]) -> Self {
        HeaderIter {
            idx: HEADER_FIRST_FEATURE,
            offset: 0, // Flags is one-indexed, but data is zero-indexed.
            flags,
            bytes,
        }
    }
}

impl<'a> Iterator for HeaderIter<'a> {
    type Item = (u32, PerfFileSection);

    fn next(&mut self) -> Option<Self::Item> {
        let start = self.idx;

        for i in start..HEADER_LAST_FEATURE {
            self.idx += 1;
            if check_flag(i, self.flags) {
                let offset = self.offset;
                self.offset += 1;

                let section: PerfFileSection = self
                    .bytes
                    .pread(mem::size_of::<PerfFileSection>() * offset)
                    .unwrap();

                return Some((i, section));
            }
        }
        None
    }
}

fn main() -> std::io::Result<()> {
    let bytes = fs::read("perf.data")?;
    let header: PerfFileHeader = bytes.pread_with(0, scroll::LE).unwrap();

    dbg!(&header);

    let opt_headers: &[u8] = &bytes[(header.data.offset + header.data.size) as usize..];

    let mut avail_cpus = 0;
    let iter = HeaderIter::new(header.flags, opt_headers);
    for (ty, section) in iter {
        let ty_name = header_to_str(ty);
        let header_bytes = extract_header_bytes(&bytes, &section);

        match ty {
            HEADER_BUILD_ID => {
                println!("{ty_name}:");
                extract_build_id(header_bytes);
            }
            HEADER_HOSTNAME | HEADER_OSRELEASE | HEADER_VERSION | HEADER_ARCH | HEADER_CPUDESC
            | HEADER_CPUID => {
                let (s, _) = extract_string_header(header_bytes);
                println!("{ty_name}: {s}");
            }
            HEADER_NRCPUS => {
                let nr_cpus: NrCpus = header_bytes.pread(0).unwrap();
                println!(
                    "{ty_name}:\n  Available: {}\n  Online:    {}",
                    nr_cpus.nr_cpus_available, nr_cpus.nr_cpus_online
                );
                avail_cpus = nr_cpus.nr_cpus_available;
            }
            HEADER_TOTAL_MEM => {
                let total_mem: u64 = header_bytes.pread(0).unwrap();
                println!("{ty_name}: {total_mem}");
            }
            HEADER_CMDLINE => {
                let (cmdline, _) = extract_string_list_header(header_bytes);
                println!("{ty_name}: {cmdline}");
            }
            HEADER_CPU_TOPOLOGY => {
                println!("{ty_name}:");
                extract_cpu_topology(header_bytes, avail_cpus);
            }
            HEADER_PMU_MAPPINGS => {
                println!("{ty_name}:");
                extract_pmu_mappings(header_bytes);
            }
            HEADER_CPU_PMU_CAPS => {
                println!("{ty_name}:");
                extract_pmu_cpu_caps(header_bytes);
            }
            HEADER_PMU_CAPS => {
                println!("{ty_name}:");
                extract_pmu_caps(header_bytes);
            }
            HEADER_GROUP_DESC => {
                extract_group_desc(header_bytes);
            }
            HEADER_STAT => {
                println!("Stat: true");
            }
            HEADER_CACHE => {
                println!("{ty_name}:");
                let levels = extract_cache(header_bytes);
                for (level, infos) in levels {
                    for info in infos {
                        println!("  L{level} {} {} [{}]", info.ty, info.size, info.map);
                        println!(
                            "    line {} sets {} ways {}",
                            info.line_size, info.sets, info.ways
                        );
                    }
                }
            }
            HEADER_SAMPLE_TIME => {
                let start_time: u64 = header_bytes.pread(0).unwrap();
                let end_time: u64 = header_bytes.pread(8).unwrap();
                println!("Sample Start Time: {start_time}\nSample End Time:   {end_time}");
            }
            HEADER_EVENT_DESC => {
                println!("{ty_name}:");
                extract_event_desc(header_bytes);
            }
            _ => {
                println!("{ty_name}: unimplemented");
                dbg!(&section);
            }
        }
    }

    let attr_size = mem::size_of::<PerfEventAttr>();
    let attr: PerfEventAttr = bytes.pread(header.attrs.offset as usize).unwrap();
    dbg!(attr);
    dbg!(header.attrs.offset + header.attr_size);
    let id_section: PerfFileSection = bytes
        .pread(header.attrs.offset as usize + attr_size)
        .unwrap();
    dbg!(&id_section);

    let ids: [u64; 4] = bytes.pread(id_section.offset as usize).unwrap();
    dbg!(ids);

    let mut offset = header.data.offset as usize;
    let mut event_count = 0;
    let end = (header.data.offset + header.data.size) as usize;
    loop {
        let event_header: PerfEventHeader = bytes.pread(offset).unwrap();
        let event_ty_name = record_to_s(event_header.ty);

        match event_header.ty {
            PERF_RECORD_MMAP => {
                let mut local_off = offset + 8;
                let pid: u32 = bytes.gread(&mut local_off).unwrap();
                let tid: u32 = bytes.gread(&mut local_off).unwrap();
                let addr: u64 = bytes.gread(&mut local_off).unwrap();
                let len: u64 = bytes.gread(&mut local_off).unwrap();
                let pgoff: u64 = bytes.gread(&mut local_off).unwrap();

                let remainder = event_header.size as usize - 16 - 24;

                let s =
                    CStr::from_bytes_until_nul(&bytes[local_off..local_off + remainder]).unwrap();
                let x: &BStr = s.to_bytes().into();
                dbg!(pid, tid, addr, len, pgoff, x);
            }
            PERF_RECORD_COMM => {
                let mut local_off = offset + 8;
                let pid: u32 = bytes.gread(&mut local_off).unwrap();
                let tid: u32 = bytes.gread(&mut local_off).unwrap();
                let remainder = event_header.size as usize - 16;

                let s =
                    CStr::from_bytes_until_nul(&bytes[local_off..local_off + remainder]).unwrap();
                let x: &BStr = s.to_bytes().into();
                dbg!(pid, tid, x);
            }
            PERF_RECORD_THREAD_MAP => {
                let mut local_off = offset + 8;

                let nr: u64 = bytes.gread(&mut local_off).unwrap();

                dbg!(nr);
                for _ in 0..nr {
                    let pid: u64 = bytes.gread(&mut local_off).unwrap();

                    let s = CStr::from_bytes_until_nul(&bytes[local_off..local_off + 16]).unwrap();
                    let x: &BStr = s.to_bytes().into();

                    dbg!(pid, x);
                }
            }
            PERF_RECORD_TIME_CONV => {
                let time_conv: PerfRecordTimeConv =
                    bytes.pread(header.data.offset as usize).unwrap();
                dbg!(time_conv);
            }
            PERF_RECORD_FINISHED_ROUND | PERF_RECORD_FINISHED_INIT => {
                // tools/perf/builtin-record.c:1497-1505
                println!("Event Type: {} - just a header", event_ty_name);
            }
            _ => println!("Event Type: {} - unimplemented", event_ty_name),
        }
        offset += event_header.size as usize;
        event_count += 1;

        if offset >= end {
            break;
        }
    }
    dbg!(event_count);

    Ok(())
}
